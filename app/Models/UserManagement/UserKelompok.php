<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserKelompok extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'kelompok_id',
    ];
    
    public function kelompok(){
        return $this->belongsTo(Kelompok::class,'kelompok_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
