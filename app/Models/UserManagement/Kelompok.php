<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelompok extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'kelas_id'
    ];

    public function userKelompok(){
        return $this->hasMany(UserKelompok::class,'kelompok_id');
    }
}
