<?php

namespace App\Models\UserManagement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;

    protected $fillable = [
        'tingkatan',
        'nama',
    ];

    public function siswa(){
        return $this->hasMany(User::class,'kelas_id')->where('role',2);
    }
}
