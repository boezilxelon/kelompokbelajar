<?php

namespace App\Models\Materi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserManagement\Kelas;

class MateriDokumen extends Model
{
    use HasFactory;

    protected $fillable = [
        'kelas_id',
        'judul',
        'file',
        'deskripsi',
    ];
    public function kelas(){
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }
}
