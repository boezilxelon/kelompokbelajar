<?php

namespace App\Models\Materi;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\UserManagement\Kelas;

class MateriYoutube extends Model
{
    use HasFactory;
    protected $fillable = [
        'kelas_id',
        'judul',
        'link',
    ];
    public function kelas(){
        return $this->belongsTo(Kelas::class, 'kelas_id');
    }
}
