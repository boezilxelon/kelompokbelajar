<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamEvent extends Model
{
    use HasFactory;
    protected $fillable = ['master_id','name','mulai','selesai'];

    public function master(){
        return $this->belongsTo(ExamMaster::class,'master_id');
    }
}
