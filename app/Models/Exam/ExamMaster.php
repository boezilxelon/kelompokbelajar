<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class ExamMaster extends Model
{
    use HasFactory;
    protected $fillable = ['name','created_by'];

    public function user(){
        return $this->belongsTo(User::class,'created_by');
    }
    
    public function question(){
        return $this->hasMany(ExamQuestion::class,'master_id');
    }
}