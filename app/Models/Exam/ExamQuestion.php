<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamQuestion extends Model
{
    use HasFactory;
    protected $fillable = ['master_id','pertanyaan','jawaban1','jawaban2','jawaban3','jawaban3','jawaban4','jawaban5','jawaban'];
}
