<?php

namespace App\Models\Exam;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Exam extends Model
{
    use HasFactory;
    protected $fillable = ['event_id','user_id','score'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function event(){
        return $this->belongsTo(ExamEvent::class,'event_id');
    }
}
