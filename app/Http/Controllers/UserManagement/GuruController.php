<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\UserManagement\UserRequest;
use App\Models\User;

class GuruController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role',1)->orderBy('name')->get();
        return view('user_management.guru',compact('users'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'         => 'unique:users,email',
            'password'      => 'min:8',
        ],[
            'email.unique'  => 'Email yang dimasukkan sudah terdaftar di sistem kami. Harap coba dengan email yang berbeda',
            'password.min'  => 'Password yang dimasukkan terlalu pendek. Harap gunakan minimal 8 karakter.',
        ]);

        User::create([
            'name'      => $request->nama,
            'email'     => $request->email,
            'password'  => $request->password,
            'role'      => 1,
        ]);
        return redirect('admin/guru')->with('success','Data Guru baru telah berhasil ditambahkan ke dalam sistem kami');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email'         => 'unique:users,email',
            'password'      => 'min:8',
        ],[
            'email.unique'  => 'Email yang dimasukkan sudah terdaftar di sistem kami. Harap coba dengan email yang berbeda',
            'password.min'  => 'Password yang dimasukkan terlalu pendek. Harap gunakan minimal 8 karakter.',
        ]);

        User::find($id)->update([
            'name'  => $request->nama,
            'email' => $request->email,
        ]);
        return redirect('admin/guru')->with('success','Data Guru telah berhasil diubah ke dalam sistem kami');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect('admin/guru')->with('success','Data Guru telah berhasil dihapus ke dalam sistem kami');
    }
}
