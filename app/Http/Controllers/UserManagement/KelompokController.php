<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserManagement\Kelompok;
use App\Models\UserManagement\UserKelompok;
use App\Models\UserManagement\Kelas;
use App\Models\User;

class KelompokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select()
    {
        $kelases = Kelas::orderBy('nama')->get();
        return view('user_management.kelompok.select',compact('kelases'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kelompoks  = Kelompok::where('kelas_id',$request->kelas_id)->get();
        $siswas     = User::where('role',2)->where('kelas_id',$request->kelas_id)->orderBy('name')->get();
        $kelas      = Kelas::find($request->kelas_id);
        return view('user_management.kelompok.index',compact('kelompoks','siswas','kelas'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kelompok = Kelompok::create([
            'name'      => $request->name,
            'kelas_id'  => $request->kelas_id,
        ]);

        foreach($request->user as $user)
        {
            $user_kelompok[] = [
                'kelompok_id'   => $kelompok->id,
                'user_id'       => $user,
            ];
        }
        UserKelompok::insert($user_kelompok);
        return back()->with('success','Data Kelompok Berahasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelompok = Kelompok::find($id);
        $kelompok->update([
            'name' =>   $request->name,
        ]);
        UserKelompok::where('kelompok_id',$kelompok->id)->delete();
        foreach($request->user as $user)
        {
            $user_kelompok[] = [
                'kelompok_id'   => $kelompok->id,
                'user_id'       => $user,
            ];
        }
        UserKelompok::insert($user_kelompok);
        return back()->with('success','Data Kelompok Berahasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kelompok::find($id)->delete();
        return back()->with('success','Data Kelompok Berhasil Dihapus');
    }
}
