<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\UserManagement\Kelas;
use App\Models\User;


class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select()
    {
        $kelases = Kelas::orderBy('nama')->get();
        return view('user_management.siswa.select',compact('kelases'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kelas    = Kelas::find($request->kelas_id);
        $users      = User::with('kelas')
            ->where('role', 2)
            ->where('kelas_id', $request->kelas_id)
            ->orderBy('name')
            ->get();
   return view('user_management.siswa.index',compact('users','kelas'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'         => 'unique:users,email',
            'password'      => 'min:8',
        ],[
            'email.unique'  => 'Email yang dimasukkan sudah terdaftar di sistem kami. Harap coba dengan email yang berbeda',
            'password.min'  => 'Password yang dimasukkan terlalu pendek. Harap gunakan minimal 8 karakter.',
        ]);

        User::create([
            'name'      => $request->nama,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'role'      => 2,
            'kelas_id'  => $request->kelas_id,
        ]);
        return back()->with('success','Data Siswa Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($id),
            ],
    
            'password' => $request->password ? 'min:8' : '',
        ],[
            'email.unique'  => 'Email yang dimasukkan sudah terdaftar di sistem kami. Harap coba dengan email yang berbeda',
            'password.min'  => 'Password yang dimasukkan terlalu pendek. Harap gunakan minimal 8 karakter.',
        ]);
        
        $user = User::find($id);
        $user->update([
            'name'      => $request->nama,
            'email'     => $request->email,
        ]);

        if($request->password)
        {
            $user->update([
                'password'  =>  bcrypt($request->password),
            ]);
        }

        return back()->with('success','Data Siswa Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return back()->with('success','Data Siswa Berhasil Dihapus');
    }
}
