<?php

namespace App\Http\Controllers\Guru\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\Exam\ExamMaster;
use App\Models\Exam\ExamEvent;
use App\Models\Exam\Exam;

class EventController extends Controller
{
    public function select()
    {
        return view('guru.exam.event-select');
    }

    public function index(Request $request)
    {
        $events  = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','>=', Carbon::now())->orderBy('mulai')->whereHas('master',function($query){
            return $query->where('created_by',Auth::user()->id);
        })->orderBy('mulai')->get();
        $dones  = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','<', Carbon::now())->orderBy('mulai')->whereHas('master',function($query){
            return $query->where('created_by',Auth::user()->id);
        })->orderBy('mulai')->get();
        $masters = ExamMaster::where('created_by',Auth::user()->id)->orderBy('name')->get();
        return view('guru.exam.event',compact('events','dones','masters'))->with('i');
    }

    public function store(Request $request)
    {
        ExamEvent::create([
            'name'       => $request->name,
            'master_id'  => $request->master_id,
            'mulai'      => $request->mulai,
            'selesai'    => $request->selesai,
        ]);
        return back()->with('success','Data Exam Master Berhasil Ditambah');
    }

    public function show($id){
        $reports = Exam::where('event_id',$id)->orderBy('score','desc')->get();
        return view('guru.exam.report',compact('reports'))->with('i');
    }

    public function update(Request $request, $id)
    {
        ExamEvent::find($id)->update([
            'name'       => $request->name,
            'master_id'  => $request->master_id,
            'mulai'      => $request->mulai,
            'selesai'    => $request->selesai,
        ]);
        return back()->with('success','Data Exam Master Berhasil Diubah');
    }

    public function destroy($id)
    {
        ExamEvent::find($id)->delete();
        return back()->with('success','Data Exam Master Berhasil Dihapus');
    }
}
