<?php

namespace App\Http\Controllers\Guru\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\Models\Exam\ExamEvent;
use App\Models\Exam\ExamQuestion;
use App\Models\Exam\ExamMaster;
use App\Models\Exam\Exam;

class ExamController extends Controller
{
    public function select()
    {
        return view('guru.exam.exam-select');
    }
    
    public function index(Request $request)
    {
        $events = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','>=', Carbon::now())->orderBy('mulai')->whereHas('master',function($query){
            return $query->where('created_by',Auth::user()->id);
        })->get();
        $dones = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','<', Carbon::now())->orderBy('mulai')->whereHas('master',function($query){
            return $query->where('created_by',Auth::user()->id);
        })->get();
        return view('guru.exam.exam',compact('events','dones'))->with('i');
    }
    public function show($id)
    {
        $exams  = Exam::all();
        $event = ExamEvent::find($id);
        if($exams->where('user_id',Auth::user()->id)->where('event_id',$event->id)->count()!=0){
            return back()->withErrors('Gagal masuk ujian');
        }else{
            return view('guru.exam.mulai',compact('event'))->with('i');
        }
    }
    public function store(Request $request){
        $jawaban     = $request->jawaban;
        $soals       = $request->soal;
        $knc_jawaban = ExamEvent::find($request->event_id)->master->question->pluck('jawaban','id')->toArray();
        $score       = 0;
        foreach($soals as $key => $soal)
        {
            $score += $knc_jawaban[$soal] == $jawaban[$key]? 1 : 0;
        }
        Exam::create([
            'event_id' => $request->event_id,
            'user_id'  => Auth::user()->id,
            'score'    => $score/count($soals)*100,
        ]);
        return redirect('guru/exam/exam')->with('success','Data ujian berhasil disimpan');
    }
}