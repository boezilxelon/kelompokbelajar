<?php

namespace App\Http\Controllers\Admin\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Exam\ExamMaster;
use App\Models\Exam\ExamEvent;
use App\Models\Exam\Exam;

class EventController extends Controller
{
    public function select()
    {
        return view('admin.exam.event-select');
    }
    public function index(Request $request)
    {
        $events = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','>=', Carbon::now())->orderBy('mulai')->get();
        $dones  = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','<', Carbon::now())->orderBy('mulai')->get();

        $masters = ExamMaster::orderBy('name')->get();
        return view('admin.exam.event',compact('events','dones','masters'))->with('i');
    }

    public function store(Request $request)
    {
        ExamEvent::create([
            'name'       => $request->name,
            'master_id'  => $request->master_id,
            'mulai'      => $request->mulai,
            'selesai'    => $request->selesai,
        ]);
        return back()->with('success','Data Event Berhasil Ditambah');
    }

    public function show($id){
        $reports = Exam::where('event_id',$id)->orderBy('score','desc')->get();
        return view('admin.exam.report',compact('reports'))->with('i');
    }

    public function update(Request $request, $id)
    {
        ExamEvent::find($id)->update([
            'name'       => $request->name,
            'master_id'  => $request->master_id,
            'mulai'      => $request->mulai,
            'selesai'    => $request->selesai,
        ]);
        return back()->with('success','Data Exam Event Berhasil Diubah');
    }

    public function destroy($id)
    {
        try {
            ExamEvent::find($id)->delete();
            return back()->with('success','Data Exam Event Berhasil Dihapus');
        } catch (\Throwable $th) {
            return back()->withErrors('Data Exam Event Tidak Dapat Dihapus');
        }
        
    }
}
