<?php

namespace App\Http\Controllers\Admin\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Exam\ExamMaster;

class MasterController extends Controller
{
    public function index()
    {
        $masters = ExamMaster::orderBy('name')->get();
        return view('admin.exam.master',compact('masters'))->with('i');
    }

    public function store(Request $request)
    {
        ExamMaster::create([
            'name'       => $request->name,
            'created_by' => Auth::user()->id,
        ]);
        return back()->with('success','Data Exam Master Berhasil Ditambah');
    }

    public function update(Request $request, $id)
    {
        ExamMaster::find($id)->update([
            'name'       => $request->name,
            'created_by' => Auth::user()->id,
        ]);
        return back()->with('success','Data Exam Master Berhasil Diubah');
    }

    public function destroy($id)
    {
        try {
            ExamMaster::find($id)->delete();
            return back()->with('success','Data Exam Master Berhasil Dihapus');
        } catch (\Throwable $th) {
            return back()->withErrors('Data Exam Master Tidak Dapat Dihapus');
        }
        
    }
}