<?php

namespace App\Http\Controllers\Admin\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Models\Exam\ExamEvent;
use App\Models\Exam\ExamQuestion;
use App\Models\Exam\Exam;

class ExamController extends Controller
{
    public function select()
    {
        return view('admin.exam.exam-select');
    }

    public function index(Request $request)
    {
        $events = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','>=', Carbon::now())->orderBy('mulai')->get();
        $dones  = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','<', Carbon::now())->orderBy('mulai')->get();
        return view('admin.exam.exam',compact('events','dones'))->with('i');
    }
    public function show($id)
    {
        $exams  = Exam::all();
        $event = ExamEvent::find($id);
        if($exams->where('user_id',Auth::user()->id)->where('event_id',$event->id)->count()!=0){
            return back()->withErrors('Gagal masuk ujian');
        }else{
            return view('admin.exam.mulai',compact('event'))->with('i');
        }
    }
    public function store(Request $request){
        $jawaban     = $request->jawaban;
        $soals       = $request->soal;
        $knc_jawaban = ExamEvent::find($request->event_id)->master->question->pluck('jawaban','id')->toArray();
        $score       = 0;
        foreach($soals as $key => $soal)
        {
            $score += $knc_jawaban[$soal] == $jawaban[$key]? 1 : 0;
        }
        Exam::create([
            'event_id' => $request->event_id,
            'user_id'  => Auth::user()->id,
            'score'    => $score/count($soals)*100,
        ]);
        return redirect('admin/exam/exam')->with('success','Data ujian berhasil disimpan');
    }
}