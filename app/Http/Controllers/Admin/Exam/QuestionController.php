<?php

namespace App\Http\Controllers\Admin\Exam;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Exam\ExamQuestion;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
        $questions = ExamQuestion::where('master_id',$request->master_id)->orderBy('id','desc')->get();
        return view('admin.exam.question',compact('questions'))->with('i');
    }

    public function store(Request $request)
    {
        ExamQuestion::create([
            'master_id' => $request->master_id,
            'pertanyaan' => $request->pertanyaan,
            'jawaban1'   => $request->jawaban1,
            'jawaban2'   => $request->jawaban2,
            'jawaban3'   => $request->jawaban3,
            'jawaban4'   => $request->jawaban4,
            'jawaban5'   => $request->jawaban5,
            'jawaban'    => $request->jawaban,
        ]);
        return back()->with('success','Data Pertanyan Berhasil Ditambah');
    }

    public function update(Request $request, $id)
    {
        ExamQuestion::find($id)->update([
            'pertanyaan' => $request->pertanyaan,
            'jawaban1'   => $request->jawaban1,
            'jawaban2'   => $request->jawaban2,
            'jawaban3'   => $request->jawaban3,
            'jawaban4'   => $request->jawaban4,
            'jawaban5'   => $request->jawaban5,
            'jawaban'    => $request->jawaban,
        ]);
        return back()->with('success','Data Exam Master Berhasil Diubah');
    }

    public function destroy($id)
    {
        ExamQuestion::find($id)->delete();
        return back()->with('success','Data Exam Master Berhasil Dihapus');
    }
}
