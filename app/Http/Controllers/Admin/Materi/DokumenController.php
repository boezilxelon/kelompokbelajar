<?php

namespace App\Http\Controllers\Admin\Materi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Models\Materi\MateriDokumen;
use App\Models\UserManagement\Kelas;

class DokumenController extends Controller
{
    public function select()
    {
        $kelases = Kelas::orderBy('nama')->get();
        return view('admin.materi.dokumen.select',compact('kelases'));
    }

    public function index(Request $request)
    {
        $dokumens = MateriDokumen::where('kelas_id',$request->kelas_id)->get();
        return view('admin.materi.dokumen.index',compact('dokumens'))->with('i');
    }

    public function store(Request $request)
    {
        $file = $request->file('file');
        $new_name = rand().'.'.$file->getClientOriginalExtension();
        $file->move(public_path('dokumen/materi/'), $new_name);

        MateriDokumen::create([
            'kelas_id'  => $request->kelas_id,
            'judul'     => $request->judul,
            'file'      => $new_name,
            'deskripsi' => $request->deskripsi,
        ]);
        return back()->with('success','Data Materi Dokumen Berhasil Ditambah');
    }

    public function update(Request $request, $id)
    {
        $materi = MateriDokumen::find($id);
        $materi->update([
            'judul'     => $request->judul,
            'deskripsi' => $request->deskripsi,
        ]);

        if($request->has('file'))
        {
            $path = public_path('dokumen/materi/'.($materi->file));
            File::delete($path);

            $file = $request->file('file');
            $new_name = rand().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('dokumen/materi/'), $new_name);
            $materi->update(['file' => $new_name]);
            
        }
        return back()->with('success','Data Materi Dokumen Berhasil Diubah');
    }

    public function destroy($id)
    {
        $materi = MateriDokumen::find($id);
        $path = public_path('dokumen/materi/'.($materi->file));
        File::delete($path);
        $materi->delete();
        return back()->with('success','Data Materi Dokumen Berhasil Dihapus');
    }
}
