<?php

namespace App\Http\Controllers\Admin\Materi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Materi\MateriYoutube;
use App\Models\UserManagement\Kelas;

class YoutubeController extends Controller
{
    public function select()
    {
        $kelases = Kelas::orderBy('nama')->get();
        return view('admin.materi.youtube.select',compact('kelases'));
    }

    public function index(Request $request)
    {
        $youtubes = MateriYoutube::where('kelas_id',$request->kelas_id)->get();
        return view('admin.materi.youtube.index',compact('youtubes'))->with('i');
    }

    public function store(Request $request)
    {
        $request->validate([
            'link' => 'required|starts_with:https://www.youtube.com/embed/',
        ],[
            'link.starts_with'      => 'Link yang dimasukkan tidak dapat disimpan. Harap masukkan link embed Youtube'
        ]);
        MateriYoutube::create([
            'kelas_id'  => $request->kelas_id,
            'judul'     => $request->judul,
            'link'      => $request->link,
        ]);
        return back()->with('success','Data Materi Youtube Berhasil Ditambah');
    }

    public function update(Request $request, $id)
    {
        MateriYoutube::find($id)->update([
            'judul'     => $request->judul,
            'link'      => $request->link,
        ]);
        return back()->with('success','Data Materi Youtube Berhasil Diubah');
    }

    public function destroy($id)
    {
        MateriYoutube::find($id)->delete();
        return back()->with('success','Data Materi Youtube Berhasil Dihapus');
    }
}