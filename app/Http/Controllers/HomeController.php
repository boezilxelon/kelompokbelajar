<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Exam\Exam;
use App\Models\Exam\ExamMaster;
use App\Models\Exam\ExamEvent;
use App\Models\Materi\MateriDokumen;
use App\Models\Materi\MateriYoutube;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        $master  = ExamMaster::count();
        $event   = ExamEvent::count();
        $dokumen = MateriDokumen::count();
        $youtube = MateriYoutube::count();
        $guru    = User::where('role',1)->count();
        $siswa    = User::where('role',2)->count();
        return view('admin.dashboard', compact('master','event','dokumen','youtube','guru','siswa'));
    }
    public function guruHome()
    {
        $master  = ExamMaster::where('created_by',Auth::user()->id)->count();
        $event  = ExamEvent::whereHas('master',function($query){
           return $query->where('created_by',Auth::user()->id);
        })->count();
        $exam    = Exam::where('user_id',Auth::user()->id)->count();
        $dokumen = MateriDokumen::whereHas('kelas', function($query){
            return $query->where('kelas_id',Auth::user()->kelas_id);
        })->count();
        return view('guru.dashboard', compact('master','event'));
    }
    public function siswaHome()
    {
        $exam    = Exam::where('user_id',Auth::user()->id)->count();
        $dokumen = MateriDokumen::whereHas('kelas', function($query){
            return $query->where('kelas_id',Auth::user()->kelas_id);
        })->count();
        $youtube = MateriYoutube::whereHas('kelas', function($query){
            return $query->where('kelas_id',Auth::user()->kelas_id);
        })->count();
            return view('siswa.dashboard', compact('exam','dokumen','youtube'));
    }
}
