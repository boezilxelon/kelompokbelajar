<?php

namespace App\Http\Controllers\Siswa\Materi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Materi\MateriYoutube;
use App\Models\UserManagement\Kelas;

class YoutubeController extends Controller
{
    public function index(Request $request)
    {
        $youtubes = MateriYoutube::where('kelas_id',Auth::user()->kelas_id)->get();
        return view('siswa.materi.youtube.index',compact('youtubes'))->with('i');
    }

}
