<?php

namespace App\Http\Controllers\Siswa\Materi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

use App\Models\Materi\MateriDokumen;
use App\Models\UserManagement\Kelas;

class DokumenController extends Controller
{
    public function index(Request $request)
    {
        $dokumens = MateriDokumen::where('kelas_id',Auth::user()->kelas_id)->get();
        return view('siswa.materi.dokumen.index',compact('dokumens'))->with('i');
    }
}
