<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Exam\ExamEvent;
use App\Models\Exam\Exam;
use Carbon\Carbon;

class ExamController extends Controller
{
    public function select()
    {
        return view('siswa.exam.exam-select');
    }

    public function index(Request $request)
    {
        $events = ExamEvent::whereYear('mulai',$request->year)->whereMonth('mulai',$request->month)->where('selesai','>=', Carbon::now())->orderBy('mulai')->get();
        $exams  = Exam::where('user_id',Auth::user()->id)->get();
        return view('siswa.exam.exam',compact('events','exams'))->with('i');
    }
    public function show($id)
    {
        $exams  = Exam::all();
        $event = ExamEvent::find($id);
        if($exams->where('user_id',Auth::user()->id)->where('event_id',$event->id)->count()!=0){
            return back()->withErrors('Gagal masuk ujian');
        }else{
            return view('siswa.exam.mulai',compact('event'))->with('i');
        }
    }
    public function store(Request $request){
        $jawaban     = $request->jawaban;
        $soals       = $request->soal;
        $knc_jawaban = ExamEvent::find($request->event_id)->master->question->pluck('jawaban','id')->toArray();
        $score       = 0;
        foreach($soals as $key => $soal)
        {
            $score += $knc_jawaban[$soal] == $jawaban[$key]? 1 : 0;
        }
        Exam::create([
            'event_id' => $request->event_id,
            'user_id'  => Auth::user()->id,
            'score'    => $score/count($soals)*100,
        ]);
        return redirect('siswa/exam')->with('success','Data ujian berhasil disimpan');
    }
}