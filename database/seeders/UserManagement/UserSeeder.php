<?php

namespace Database\Seeders\UserManagement;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\UserManagement\Kelas;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kelas = Kelas::create([
            'tingkatan'      => 7,
            'nama'           => 'A',
        ]);
        
        $users = [
            [
               'name'        =>'Admin',
               'email'       =>'admin@mail.com',
               'role'        => 0,
               'password'    => bcrypt('password'),
               'kelas_id'    => null,
            ],
            [
               'name'        =>'Guru',
               'email'       =>'guru@mail.com',
               'role'        => 1,
               'password'    => bcrypt('password'),
               'kelas_id'    => null,
            ],
            [
               'name'        =>'Siswa',
               'email'       =>'siswa@mail.com',
               'role'        => 2,
               'password'    => bcrypt('password'),
               'kelas_id'    => $kelas->id,
            ],
            
        ];
    
        foreach ($users as $key             => $user) 
        {
            User::create($user);
        }
    }
}