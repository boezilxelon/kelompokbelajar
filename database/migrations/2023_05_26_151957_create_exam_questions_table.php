<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('master_id');
            $table->longText('pertanyaan');
            $table->longText('jawaban1');
            $table->longText('jawaban2');
            $table->longText('jawaban3');
            $table->longText('jawaban4');
            $table->longText('jawaban5');
            $table->integer('jawaban');
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('exam_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_questions');
    }
};
