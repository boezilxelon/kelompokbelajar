<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('master_id');
            $table->string('name');
            $table->dateTime('mulai');
            $table->dateTime('selesai');
            $table->timestamps();

            $table->foreign('master_id')->references('id')->on('exam_masters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_events');
    }
};
