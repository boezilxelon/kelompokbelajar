<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});


Auth::routes();

// Route::middleware(['auth','user-role:Admin'])->group(function()
Route::prefix('admin')->middleware(['auth', 'user-role:Admin'])->as('admin.')->group(function ()
    {
    Route::get("home",[App\Http\Controllers\HomeController::class, 'adminHome'])->name("home");

    Route::prefix('kelompok')->as('kelompok.')->group(function ()
    {
        Route::get("select",[App\Http\Controllers\UserManagement\KelompokController::class,'select'])->name('select');
        Route::resource("index",App\Http\Controllers\UserManagement\KelompokController::class);
    });

    Route::prefix('siswa')->as('siswa.')->group(function ()
    {
        Route::get("select",[App\Http\Controllers\UserManagement\SiswaController::class,'select'])->name('select');
        Route::resource("index",App\Http\Controllers\UserManagement\SiswaController::class);
    });

    Route::resource("guru",App\Http\Controllers\UserManagement\GuruController::class);
    Route::resource("kelas",App\Http\Controllers\UserManagement\KelasController::class);
    
    Route::prefix('materi')->as('materi.')->group(function ()
    {
        Route::get("dokumen/select",[App\Http\Controllers\Admin\Materi\DokumenController::class,'select'])->name('dokumen.select');
        Route::resource("dokumen",App\Http\Controllers\Admin\Materi\DokumenController::class);
        Route::get("youtube/select",[App\Http\Controllers\Admin\Materi\YoutubeController::class,'select'])->name('youtube.select');
        Route::resource("youtube",App\Http\Controllers\Admin\Materi\YoutubeController::class);
        
    });

    Route::prefix('exam')->as('exam.')->group(function ()
    {
        Route::resource("master",App\Http\Controllers\Admin\Exam\MasterController::class);
        Route::resource("question",App\Http\Controllers\Admin\Exam\QuestionController::class);
        Route::resource("event",App\Http\Controllers\Admin\Exam\EventController::class);
        Route::get("event-select",[App\Http\Controllers\Admin\Exam\EventController::class,'select'])->name('event.select');
        Route::resource("exam",App\Http\Controllers\Admin\Exam\ExamController::class);
        Route::get("exam-select",[App\Http\Controllers\Admin\Exam\ExamController::class,'select'])->name('exam.select');
    });
});

// Route Guru
Route::prefix('guru')->middleware(['auth', 'user-role:Guru'])->as('guru.')->group(function ()
{
    Route::get("home",[App\Http\Controllers\HomeController::class, 'guruHome'])->name("home");

    Route::prefix('exam')->as('exam.')->group(function ()
    {
        Route::resource("master",App\Http\Controllers\Guru\Exam\MasterController::class);
        Route::resource("question",App\Http\Controllers\Guru\Exam\QuestionController::class);
        Route::resource("event",App\Http\Controllers\Guru\Exam\EventController::class);
        Route::get("event-select",[App\Http\Controllers\Guru\Exam\EventController::class,'select'])->name('event.select');
        Route::resource("exam",App\Http\Controllers\Guru\Exam\ExamController::class);
        Route::get("exam-select",[App\Http\Controllers\Guru\Exam\ExamController::class,'select'])->name('exam.select');
    });

    Route::prefix('materi')->as('materi.')->group(function ()
    {
        Route::get("dokumen/select",[App\Http\Controllers\Guru\Materi\DokumenController::class,'select'])->name('dokumen.select');
        Route::resource("dokumen",App\Http\Controllers\Guru\Materi\DokumenController::class);
        Route::get("youtube/select",[App\Http\Controllers\Guru\Materi\YoutubeController::class,'select'])->name('youtube.select');
        Route::resource("youtube",App\Http\Controllers\Guru\Materi\YoutubeController::class);
    });
});
// Route Siswa
Route::prefix('siswa')->middleware(['auth', 'user-role:Siswa'])->as('siswa.')->group(function ()
{
    Route::get("home",[App\Http\Controllers\HomeController::class, 'siswaHome'])->name("home");
    Route::resource("exam",App\Http\Controllers\Siswa\ExamController::class);
    Route::get("exam-select",[App\Http\Controllers\Siswa\ExamController::class,'select'])->name('exam.select');

    Route::prefix('materi')->as('materi.')->group(function ()
    {
        Route::resource("dokumen",App\Http\Controllers\Siswa\Materi\DokumenController::class);
        Route::resource("youtube",App\Http\Controllers\Siswa\Materi\YoutubeController::class);
    });
});