<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{asset('vendors/animate.css/animate.min.css')}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    @if(session('error'))
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Login Gagal</strong> Pastikan email dan password benar.
        </div>
        <div>
    @endif
    
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                
            <section class="login_content">
                <h3><i class="fa fa-graduation-cap"></i> Kelompok Belajar</h3>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <h1>Login Form</h1>
                    <div>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email"/>
                    </div>
                    <div>
                        <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password" placeholder="Password" />
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn btn-secondary" type="submit">Login</button>
                            </div>
                            <div class="col-sm-6">
                                <a href="#signup" class="to_register"> <button class="btn btn-secondary" type="button">Create Account </button> </a>
                            </div>
                        </div>
                    </div>
                    <div class="separator">
                        <div class="clearfix"></div>
                        <div>
                            <p>©2023 Teknik Informatika Politeknik Negeri Indramayu</p>
                        </div>
                    </div>
                </form>
            </section>
            </div>

            <div id="register" class="animate form registration_form">
            <section class="login_content">
                <h3><i class="fa fa-graduation-cap"></i> Kelompok Belajar</h3>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <h1>Create Account</h1>
                <div>
                    <input type="text" name="name" class="form-control" placeholder="Name" required="" />
                </div>
                <div>
                    <input type="email" name="email" class="form-control" placeholder="Email" required="" />
                </div>
                <div>
                    <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                </div>
                <div>
                    <div class="row">
                        <div class="col-sm-6">
                            <button class="btn btn-secondary" type="submit">Register</button>
                        </div>
                        <div class="col-sm-6">
                            <a href="#signin" class="to_register"> <button class="btn btn-secondary" type="button">Already a member</button> </a>
                        </div>
                    </div>
                </div>
                <div class="separator">
                    <div class="clearfix"></div>
                    <div>
                        <p>©2023 Teknik Informatika Politeknik Negeri Indramayu</p>
                    </div>
                </div>
                </form>
            </section>
            </div>
        </div>
    </div>
  </body>
</html>
