@extends('template.app')
@section('content')
<div class="col-md-6 col-sm-6  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Select Data Exam</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

      <form id="demo-form2" action="{{route('siswa.exam.index')}}" method="GET" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Tahun <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <select name="year" id="year" class="form-control">
                        @for ($i=(Carbon\Carbon::now()->year+1); $i>=2000; $i--)
                          <option value="{{$i}}" @selected(date("Y") == $i)>{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="month">Bulan <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                  <select name="month" id="month" class="form-control">
                    <option value="01"@selected(date("m") == 1)>Januari</option>
                    <option value="02"@selected(date("m") == 2)>Februari</option>
                    <option value="03"@selected(date("m") == 3)>Maret</option>
                    <option value="04"@selected(date("m") == 4)>April</option>
                    <option value="05"@selected(date("m") == 5)>Mei</option>
                    <option value="06"@selected(date("m") == 6)>Juni</option>
                    <option value="07"@selected(date("m") == 7)>Juli</option>
                    <option value="08"@selected(date("m") == 8)>Agustus</option>
                    <option value="09"@selected(date("m") == 9)>September</option>
                    <option value="10"@selected(date("m") == 10)>Oktober</option>
                    <option value="11"@selected(date("m") == 11)>November</option>
                    <option value="12"@selected(date("m") == 12)>Desember</option>
                  </select>                
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

      </div>
    </div>
  </div>
@endsection