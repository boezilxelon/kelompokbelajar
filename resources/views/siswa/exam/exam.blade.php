@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Event</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;" width="5%">No</th>
                                                <th style="text-align: center;">Title</th>
                                                <th style="text-align: center;">Master Data</th>
                                                <th style="text-align: center;">Mulai</th>
                                                <th style="text-align: center;">Selesai</th>
                                                <th style="text-align: center; width:22%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($events as $event)
                                            <tr>
                                                <td style="text-align: center;">{{++$i}}</td>
                                                <td style="text-align: center;">{{$event->name}}</td>
                                                <td style="text-align: center;">{{$event->master->name}} Soal</td>
                                                <td style="text-align: center;">{{$event->mulai}}</td>
                                                <td style="text-align: center;">{{$event->selesai}}</td>
                                                <td style="text-align: center;">
                                                    <a href="{{ route('siswa.exam.show',$event->id)}}">
                                                        <button type="button" class="btn btn-success"><i class="fa fa-play-circle"></i> Mulai</button>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Event</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable2" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;" width="5%">No</th>
                                                <th style="text-align: center;">Title</th>
                                                <th style="text-align: center;">Mulai</th>
                                                <th style="text-align: center;">Store</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i=0;
                                            @endphp
                                            @foreach ($exams as $exam)
                                            <tr>
                                                <td style="text-align: center;">{{++$i}}</td>
                                                <td style="text-align: center;">{{$exam->event->master->name}} Soal</td>
                                                <td style="text-align: center;">{{$exam->event->mulai}} Soal</td>
                                                <td style="text-align: center;">{{$exam->score}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_content bs-example-popovers"></div>
    </div>
@endsection

@push('js')
    <script>
        $('#datatable2').DataTable();
    </script>
@endpush