@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Materi</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi Youtube</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Judul</th>
                                            <th style="text-align: center; width:24%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($youtubes as $youtube)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$youtube->judul}}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-center">
                                                    <button type="button" class="btn btn-secondary"  data-toggle="modal" data-target="#modal-edit-lg{{'youtube'.$youtube->id}}"><i class="	fa fa-play-circle"></i> Video</button>
                                                </div>
                                            </td>
                                        </tr>                   
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach ($youtubes as $youtube)        
        <x-box-modal-edit-large id="{{'youtube'.$youtube->id}}">
            @slot('title') {{$youtube->judul}} @endslot
            @slot('body')
                <iframe id="video{{$youtube->id}}" width="100%" height="500px" src="{{$youtube->link}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection
@push('js')
    <script>
        $(document).ready(function(){
            $(".modal").on("hidden.bs.modal", function(){
                $(this).find('iframe').each(function(){
                    var src = $(this).attr('src');
                    $(this).attr('src',src);
                });
            });
        });
    </script>
@endpush