@extends('template.app')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h4>Materi</h4>
        </div>
    </div>

    <div class="x_content bs-example-popovers">

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi Dokumen</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Judul</th>
                                            <th style="text-align: center;">Deskripsi</th>
                                            <th style="text-align: center; width:24%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dokumens as $dokumen)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$dokumen->judul}}</td>
                                            <td style="text-align: center;">{!! $dokumen->deskripsi !!}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-center">
                                                  <a href="{{ asset('dokumen/materi/' . $dokumen->file) }}" class="btn btn-secondary" download>
                                                    <i class="fa fa-download"></i> Download
                                                  </a>
                                                </div>
                                            </td>                                              
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@include('template.editor-one')