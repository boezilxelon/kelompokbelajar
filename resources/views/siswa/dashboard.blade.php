@extends('template.app')

@section('content')
    <div class="row">
        <div class="col-md-2">
        <div class="tile_count">
            <div class="tile_stats_count">
            <span class="count_top"><i class="fa fa-newspaper-o"></i> Total Ujian</span>
            <div class="count">{{$exam}}</div>
            <span class="count_bottom">Kali Mengikuti</span>
            </div>
        </div>
        </div>

        <div class="col-md-2">
        <div class="tile_count">
            <div class="tile_stats_count">
            <span class="count_top"><i class="fa fa-book"></i> Total Materi Dokumen</span>
            <div class="count">{{$dokumen}}</div>
            <span class="count_bottom">File</span>
            </div>
        </div>
        </div>

        <div class="col-md-2">
        <div class="tile_count">
            <div class="tile_stats_count">
            <span class="count_top"><i class="fa fa-video"></i> Total Materi youtube</span>
            <div class="count">{{$youtube}}</div>
            <span class="count_bottom">Video</span>
            </div>
        </div>
        </div>
    </div>  
@endsection
