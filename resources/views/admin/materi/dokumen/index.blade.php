@extends('template.app')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h4>Materi</h4>
        </div>
    </div>

    <div class="x_content bs-example-popovers">

    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi Dokumen</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg"
                                    style="float:right;">
                                    <i class="fa fa-plus"></i> Tambah Materi Dokumen
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Judul</th>
                                            <th style="text-align: center;">Deskripsi</th>
                                            <th style="text-align: center; width:24%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dokumens as $dokumen)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$dokumen->judul}}</td>
                                            <td style="text-align: center;">{!! $dokumen->deskripsi !!}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <a href="{{ asset('dokumen/materi/' . $dokumen->file) }}" class="btn btn-secondary" download>
                                                        <i class="fa fa-download"></i> Download
                                                    </a>

                                                        <button type="button" class="btn btn-warning text-white"
                                                            data-toggle="modal"
                                                            data-target="#modal-edit-lg{{$dokumen->id}}"><i
                                                                class="fa fa-pencil"></i> Ubah</button>
                                                    <form
                                                        action="{{route('admin.materi.dokumen.destroy', $dokumen->id)}}"
                                                        method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i
                                                                class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Materi Dokumen @endslot

        @slot('body')
        <form id="demo-form2" action="{{route('admin.materi.dokumen.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Judul <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="text" id="judul" name="judul" required="required" class="form-control ">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">File Dokumen <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="file" id="file" name="file" required="required" class="form-control ">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Deskripsi <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a class="font-size" data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a></li>
                                <li><a class="font-size" data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a></li>
                                <li><a class="font-size" data-edit="fontSize 1"><p style="font-size:11px">Small</p></a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i
                                    class="fa fa-italic"></i></a>
                            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i
                                    class="fa fa-strikethrough"></i></a>
                            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i
                                    class="fa fa-underline"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i
                                    class="fa fa-list-ul"></i></a>
                            <a class="btn" data-edit="insertorderedlist" title="Number list"><i
                                    class="fa fa-list-ol"></i></a>
                            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i
                                    class="fa fa-dedent"></i></a>
                            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i
                                    class="fa fa-align-left"></i></a>
                            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i
                                    class="fa fa-align-center"></i></a>
                            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i
                                    class="fa fa-align-right"></i></a>
                            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i
                                    class="fa fa-align-justify"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                        </div>
                    </div>

                    <div id="editor-one" class="editor-wrapper"></div>
                    <textarea name="deskripsi" id="deskripsi" style="display:none;"></textarea>

                </div>
            </div>
            <input type="hidden" name="kelas_id" value="{{app('request')->input('kelas_id')}}">
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info" style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary" style="float: right;"
                        data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
        @endslot

    </x-box-modal-large>

    @foreach ($dokumens as $dokumen)
    <x-box-modal-edit-large id="{{$dokumen->id}}">
        @slot('title') Edit Materi Dokumen @endslot
        @slot('body')
        <form id="demo-form2" action="{{route('admin.materi.dokumen.update',$dokumen->id)}}" method="POST"
            data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Judul <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="link" id="judul" name="judul" required="required" class="form-control "
                        value="{{$dokumen->judul}}">
                </div>
            </div>

            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">File Dokumen <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="file" id="file" name="file" class="form-control">
                </div>
            </div>

            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Deskripsi <span
                        class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor-one">
                        <div class="btn-group">
                            <a class="btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a class="font-size" data-edit="fontSize 5"><p style="font-size:17px">Huge</p></a></li>
                                <li><a class="font-size" data-edit="fontSize 3"><p style="font-size:14px">Normal</p></a></li>
                                <li><a class="font-size" data-edit="fontSize 1"><p style="font-size:11px">Small</p></a></li>
                            </ul>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                            <a class="btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i
                                    class="fa fa-italic"></i></a>
                            <a class="btn" data-edit="strikethrough" title="Strikethrough"><i
                                    class="fa fa-strikethrough"></i></a>
                            <a class="btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i
                                    class="fa fa-underline"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i
                                    class="fa fa-list-ul"></i></a>
                            <a class="btn" data-edit="insertorderedlist" title="Number list"><i
                                    class="fa fa-list-ol"></i></a>
                            <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i
                                    class="fa fa-dedent"></i></a>
                            <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i
                                    class="fa fa-align-left"></i></a>
                            <a class="btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i
                                    class="fa fa-align-center"></i></a>
                            <a class="btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i
                                    class="fa fa-align-right"></i></a>
                            <a class="btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i
                                    class="fa fa-align-justify"></i></a>
                        </div>

                        <div class="btn-group">
                            <a class="btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                            <a class="btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                        </div>
                    </div>

                    <div id="edit_editor-one" class="editor-wrapper">{!! html_entity_decode($dokumen->deskripsi) !!}</div>
                    <textarea name="deskripsi" id="edit_deskripsi" style="display:none;"></textarea>

                </div>
            </div>
            <div class="ln_solid"></div>

            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info" style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary" style="float: right;"
                        data-dismiss="modal">Close</button>
                </div>
            </div>

        </form>
        @endslot
    </x-box-modal-edit-large>
    @endforeach
@endsection

@include('template.editor-one')