@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-6 col-sm-6  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Select Data Event</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="demo-form2" action="{{route('admin.exam.exam.index')}}" method="GET" data-parsley-validate class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Tahun <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <select name="year" id="year" class="form-control">
                                        @for ($year=(Carbon\Carbon::now()->year+1); $year>=2000; $year--)
                                        <option value="{{$year}}" @selected(app('request')->input('year') == $year)>{{$year}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="month">Bulan <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                <select name="month" id="month" class="form-control">
                                    <option value="01"@selected(app('request')->input('month') == 1)>Januari</option>
                                    <option value="02"@selected(app('request')->input('month') == 2)>Februari</option>
                                    <option value="03"@selected(app('request')->input('month') == 3)>Maret</option>
                                    <option value="04"@selected(app('request')->input('month') == 4)>April</option>
                                    <option value="05"@selected(app('request')->input('month') == 5)>Mei</option>
                                    <option value="06"@selected(app('request')->input('month') == 6)>Juni</option>
                                    <option value="07"@selected(app('request')->input('month') == 7)>Juli</option>
                                    <option value="08"@selected(app('request')->input('month') == 8)>Agustus</option>
                                    <option value="09"@selected(app('request')->input('month') == 9)>September</option>
                                    <option value="10"@selected(app('request')->input('month') == 10)>Oktober</option>
                                    <option value="11"@selected(app('request')->input('month') == 11)>November</option>
                                    <option value="12"@selected(app('request')->input('month') == 12)>Desember</option>
                                </select>                
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Exam</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center;" width="5%">No</th>
                                                <th style="text-align: center;">Title</th>
                                                <th style="text-align: center;">Master Data</th>
                                                <th style="text-align: center;">Mulai</th>
                                                <th style="text-align: center;">Selesai</th>
                                                <th style="text-align: center;">Status</th>
                                                <th style="text-align: center; width:22%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($events as $event)
                                                <tr>
                                                    <td style="text-align: center;">{{++$i}}</td>
                                                    <td style="text-align: center;">{{$event->name}}</td>
                                                    <td style="text-align: center;">{{$event->master->name}} Soal</td>
                                                    <td style="text-align: center;">{{$event->mulai}}</td>
                                                    <td style="text-align: center;">{{$event->selesai}}</td>
                                                    <td style="text-align: center;"><span class="badge badge-success">Belum Selesai</span></td>
                                                    <td style="text-align: center;">
                                                            <a href="{{ route('admin.exam.exam.show',$event->id)}}">
                                                                <button type="button" class="btn btn-success"><i class="fa fa-play-circle"></i> Mulai</button>
                                                            </a>
                                                        <a href="{{route('admin.exam.event.show',$event->id)}}"><button class="btn btn-info"><i class="fa fa-book"></i> Hasil</button></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            @foreach ($dones as $event)
                                                <tr>
                                                    <td style="text-align: center;">{{++$i}}</td>
                                                    <td style="text-align: center;">{{$event->name}}</td>
                                                    <td style="text-align: center;">{{$event->master->name}} Soal</td>
                                                    <td style="text-align: center;">{{$event->mulai}}</td>
                                                    <td style="text-align: center;">{{$event->selesai}}</td>
                                                    <td style="text-align: center;"><span class="badge badge-danger">Selesai</span></td>
                                                    <td style="text-align: center;">
                                                        <a href="{{route('admin.exam.event.show',$event->id)}}"><button class="btn btn-info"><i class="fa fa-book"></i> Hasil</button></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="x_content bs-example-popovers"></div>
    </div>
@endsection