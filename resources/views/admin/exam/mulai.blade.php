<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kelompok Belajar</title>
    <link href="{{asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    <link href="{{asset('build/css/custom.min.css')}}" rel="stylesheet">
    <style>
        section {
          display: none;
        }
    
        section:target {
          display: block;
        }
    </style>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="row" style="padding: 5%">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{$event->name}}</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="btn-group mr-2" role="group" aria-label="First group">
                                    @foreach ($event->master->question as $key=> $question)
                                        <a href="#soal{{$key+1}}"><button type="button" class="btn btn-secondary">{{$key+1}}</button></a>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h5 id="waktu" style="float: right;"></h5>
                            </div>
                            <div class="col-sm-12">
                                <form action="{{route('admin.exam.exam.store')}}" method="POST">
                                    @csrf
                                    @foreach ($event->master->question->shuffle() as $key=> $question)
                                        <section id="soal{{$key+1}}"><div class="bs-example" data-example-id="simple-jumbotron">
                                            <div class="jumbotron">
                                                <h3>{{$key+1}}. {!!$question->pertanyaan!!}</h1>
                                                    <input type="hidden" name="jawaban[{{$key}}]" value="null">
                                                    <input type="hidden" name="soal[{{ $key }}]" value="{{$question->id}}">
                                                    <h4><input type="radio" value="1" id="soal{{$key}}_1" name="jawaban[{{$key}}]"> {{$question->jawaban1}}</h4><br>
                                                    <h4><input type="radio" value="2" id="soal{{$key}}_2" name="jawaban[{{$key}}]"> {{$question->jawaban2}}</h4><br>
                                                    <h4><input type="radio" value="3" id="soal{{$key}}_3" name="jawaban[{{$key}}]"> {{$question->jawaban3}}</h4><br>
                                                    <h4><input type="radio" value="4" id="soal{{$key}}_4" name="jawaban[{{$key}}]"> {{$question->jawaban4}}</h4><br>
                                                    <h4><input type="radio" value="5" id="soal{{$key}}_5" name="jawaban[{{$key}}]"> {{$question->jawaban5}}</h4><br>
                                            </div>
                                            <div style="float: right;">
                                                <a href="#soal{{$key}}"><button type="button" class="btn btn-info">Kembali</button></a>
                                                @if($key+1 != count($event->master->question))
                                                    <a href="#soal{{$key+2}}"><button type="button" class="btn btn-success">Lanjut</button></a>
                                                @else
                                                    <button type="submit" class="btn btn-success" id="selesai">Selesai</button>
                                                @endif
                                            </div>
                                        </section>
                                    @endforeach
                                    <input type="hidden" name="event_id" value="{{$event->id}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>
        <script src="{{asset('vendors/nprogress/nprogress.js')}}"></script>
        <script src="{{asset('vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
        <script src="{{asset('build/js/custom.min.js')}}"></script>
        <script src="{{asset('vendors/jquery-steps/build/jquery.steps.min.js')}}"></script>
        <script>
            if (!window.location.hash) {
              window.location.hash = 'soal1';
            }
        </script>
        <script>
            var targetDate = new Date("{{$event->selesai}}").getTime();
            var countdown = setInterval(function() {
                var now = new Date().getTime();
                var difference = targetDate - now;
                var days = Math.floor(difference / (1000 * 60 * 60 * 24));
                var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((difference % (1000 * 60)) / 1000);
                var waktuText = (days ? days + " hari : " : "") + hours + " jam : " + minutes + " menit : " + seconds + " detik";
                $('#waktu').text(waktuText);
                if (difference < 0) {
                    clearInterval(countdown); // Menghentikan interval
                    document.getElementById("selesai").click();
                }
            }, 1000);
        </script>
      
    </body>
</html>
