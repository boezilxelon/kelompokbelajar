@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Data Pertanyaan</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah Pertanyaan
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Soal</th>
                                            <th style="text-align: center;">Jawaban 1</th>
                                            <th style="text-align: center;">Jawaban 2</th>
                                            <th style="text-align: center;">Jawaban 3</th>
                                            <th style="text-align: center;">Jawaban 4</th>
                                            <th style="text-align: center;">Jawaban 5</th>
                                            <th style="text-align: center;">Jawaban</th>
                                            <th style="text-align: center; width:26%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($questions as $question)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{!! $question->pertanyaan !!}</td>
                                            <td style="text-align: center;">{{$question->jawaban1}}</td>
                                            <td style="text-align: center;">{{$question->jawaban2}}</td>
                                            <td style="text-align: center;">{{$question->jawaban3}}</td>
                                            <td style="text-align: center;">{{$question->jawaban4}}</td>
                                            <td style="text-align: center;">{{$question->jawaban5}}</td>
                                            <td style="text-align: center;">{{$question->jawaban}}</td>
                                            <td style="text-align: center; width:16%">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$question->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('admin.exam.question.destroy', $question->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Exam Master @endslot
        @slot('body')
            <form id="demo-form2" action="{{route('admin.exam.question.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Soal <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <textarea name="pertanyaan" cols="10" class="form-control"></textarea>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 1 <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="jawaban1" name="jawaban1" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 2 <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="jawaban2" name="jawaban2" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 3 <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="jawaban3" name="jawaban3" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 4 <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="jawaban4" name="jawaban4" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 5 <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="jawaban5" name="jawaban5" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <select name="jawaban" id="jawaban" required="required" class="form-control">
                            <option value="1">Jawaban 1</option>
                            <option value="2">Jawaban 2</option>
                            <option value="3">Jawaban 3</option>
                            <option value="4">Jawaban 4</option>
                            <option value="5">Jawaban 5</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="master_id" value="{{app('request')->input('master_id')}}">
                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        @endslot
    </x-box-modal-large>

    @foreach ($questions as $question)
        <x-box-modal-edit-large id="{{$question->id}}">
            @slot('title') Edit Pertanyaan @endslot
            @slot('body')
                <form id="demo-form2" action="{{route('admin.exam.question.update',$question->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                    @csrf
                    @method('PUT')
                    
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Soal <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <textarea name="pertanyaan" cols="10" class="form-control">{{$question->pertanyaan}}</textarea>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 1 <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="jawaban1" name="jawaban1" required="required" class="form-control" value="{{$question->jawaban1}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 2 <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="jawaban2" name="jawaban2" required="required" class="form-control" value="{{$question->jawaban2}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 3 <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="jawaban3" name="jawaban3" required="required" class="form-control " value="{{$question->jawaban3}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 4 <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="jawaban4" name="jawaban4" required="required" class="form-control" value="{{$question->jawaban4}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban 5 <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="jawaban5" name="jawaban5" required="required" class="form-control" value="{{$question->jawaban5}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Jawaban <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <select name="jawaban" id="jawaban" required="required" class="form-control">
                                <option @selected($question->jawaban == 1) value="1">Jawaban 1</option>
                                <option @selected($question->jawaban == 2) value="2">Jawaban 2</option>
                                <option @selected($question->jawaban == 3) value="3">Jawaban 3</option>
                                <option @selected($question->jawaban == 4) value="4">Jawaban 4</option>
                                <option @selected($question->jawaban == 5) value="5">Jawaban 5</option>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                            <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection
@include('template.editor-one')