@extends('template.app')

@section('content')
  <div class="row" style="display: inline-block;">
      <div class="tile_count">
          <div class="col-md-3 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Guru</span>
              <div class="count">{{$guru}}</div>
          </div>
          <div class="col-md-3 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Total Siswa</span>
              <div class="count">{{$siswa}}</div>
          </div>
          <div class="col-md-3 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Materi Dokumen</span>
              <div class="count">{{$dokumen}}</div>
          </div>
          <div class="col-md-3 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Exam Event</span>
              <div class="count">{{$youtube}}</div>
          </div>
          <div class="col-md-2 col-sm-4  tile_stats_count">
              
          </div>
          <div class="col-md-2 col-sm-4  tile_stats_count">
              
          </div>
      </div>
  </div>
  <!-- /top tiles -->
@endsection
