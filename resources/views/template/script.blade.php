
    <!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('vendors/fastclick/lib/fastclick.js')}}"></script>

    <!-- bootstrap-wysiwyg -->
	<script src="{{asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
	<script src="{{asset('vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
	<script src="{{asset('vendors/google-code-prettify/src/prettify.js')}}"></script>


    <!-- Datatables -->
    <script src="{{asset('vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

    <!-- Select2 -->
	<script src="{{asset('vendors/select2/dist/js/select2.full.min.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{asset('build/js/custom.min.js')}}"></script>

    <script>
        setTimeout(function() {
            $('.alert').fadeOut('fast');
        }, 3000); // 300 milidetik atau 3 detik
    </script>

    @stack('js');