@push('js')
    <script>
        $('#editor-one').on('input', function() {
            // Mendapatkan konten editor
            var content = $(this).html();
            // Mengatur nilai textarea tersembunyi dengan konten yang diperbarui
            $('#deskripsi').val(content);
        });

        // Submit form
        $('form').submit(function() {
            // Mendapatkan konten editor
            var content = $('#editor-one').html();
            // Mengatur nilai textarea tersembunyi dengan konten yang diperbarui
            $('#deskripsi').val(content);
            return true;
        });
    </script>

    <script>
        $('#edit_editor-one').on('input', function() {
            // Mendapatkan konten editor
            var content = $(this).html();
            // Mengatur nilai textarea tersembunyi dengan konten yang diperbarui
            $('#edit_deskripsi').val(content);
        });

        // Submit form
        $('form').submit(function() {
            // Mendapatkan konten editor
            var content = $('#edit_editor-one').html();
            // Mengatur nilai textarea tersembunyi dengan konten yang diperbarui
            $('#edit_deskripsi').val(content);
            return true;
        });
    </script>
@endpush