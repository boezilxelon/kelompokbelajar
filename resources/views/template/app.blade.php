<!DOCTYPE html>
<html lang="en">
    @include('template.head')
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                @include('template.sidebar')

                @include('template.header')

                <div class="right_col" role="main">

                    {{-- MAIN CONTENT --}}
                    <div class="x_content bs-example-popovers">
                        @if(session('success'))
                            <div class="alert alert-success alert-dismissible " role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>{{ session('success') }}</strong>
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible " role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                    @foreach ($errors->all() as $error)
                                        <strong>- {{ $error}}</strong> <br>
                                    @endforeach
                            </div>
                        @endif
                    </div>

                    @yield('content') 
                
                </div>
            </div>
        </div>
        @include('template.footer')
        @include('template.script')
    </body>
</html>
