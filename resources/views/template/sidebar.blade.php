<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><span>Kelompok Belajar</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{asset('images/img.jpg')}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{Auth::user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('login')}}">Dashboard</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-newspaper-o"></i>Try Out<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @if(Auth::user()->role == 'Admin')
                                <li><a href="{{route('admin.exam.master.index')}}">Master</a></li>
                                <li><a href="{{route('admin.exam.event.select')}}">Event</a></li>
                                <li><a href="{{route('admin.exam.exam.select')}}">Exam</a></li>
                            @elseif(Auth::user()->role == 'Guru')
                                <li><a href="{{route('guru.exam.master.index')}}">Master</a></li>
                                <li><a href="{{route('guru.exam.event.select')}}">Event</a></li>
                                <li><a href="{{route('guru.exam.exam.select')}}">Exam</a></li>
                            @else
                                <li><a href="{{route('siswa.exam.select')}}">Exam</a></li>
                            @endif
                        </ul>
                    </li>
                    <li><a><i class="fa fa-book"></i> Materi<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @if(Auth::user()->role == 'Admin')
                                <li><a href="{{route('admin.materi.dokumen.select')}}">Dokumen</a></li>
                                <li><a href="{{route('admin.materi.youtube.select')}}">Youtube</a></li>
                            @elseif(Auth::user()->role == 'Guru')
                                <li><a href="{{route('guru.materi.dokumen.select')}}">Dokumen</a></li>
                                <li><a href="{{route('guru.materi.youtube.select')}}">Youtube</a></li>
                            @else
                                <li><a href="{{route('siswa.materi.dokumen.index')}}">Dokumen</a></li>
                                <li><a href="{{route('siswa.materi.youtube.index')}}">Youtube</a></li>
                            @endif
                        </ul>
                    </li>

                    @if(Auth::user()->role == 'Admin')
                    <li><a><i class="fa fa-users"></i> User Management <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('admin.kelompok.select')}}">Kelompok</a></li>
                            <li><a href="{{route('admin.siswa.select')}}">Siswa</a></li>
                            <li><a href="{{route('admin.guru.index')}}">Guru</a></li>
                            <li><a href="{{route('admin.kelas.index')}}">Kelas</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>