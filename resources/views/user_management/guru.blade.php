@extends('template.app')
@section('content')
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h4>User Management</h4>
        </div>
    </div>

    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
            <div class="x_title">
                <h2>List Guru</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box" style="padding-right:10px">
                            <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                               <i class="fa fa-plus"></i> Tambah Guru
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;" width="5%">No</th>
                                        <th style="text-align: center;">Name</th>
                                        <th style="text-align: center;">Email</th>
                                        <th style="text-align: center;">Role</th>
                                        <th style="text-align: center; width:17%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td style="text-align: center;">{{++$i}}</td>
                                        <td style="text-align: center;">{{$user->name}}</td>
                                        <td style="text-align: center;">{{$user->email}}</td>
                                        <td style="text-align: center;">{{$user->role}}</td>
                                        <td style="text-align: center;">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$user->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                <form action="{{route('admin.guru.destroy', $user->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                </form>
                                            </div>
                                                            
                                        </td>
                                    </tr>                   
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<x-box-modal-large>
    @slot('title') Tambah Guru @endslot

    @slot('body')
    <form id="demo-form2" action="{{route('admin.guru.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
        @csrf
        <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Nama Lengkap <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-9 ">
                <input type="text" id="nama" name="nama" required="required" class="form-control ">
            </div>
        </div>

        <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">Email <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-9 ">
                <input type="email" id="email" name="email" required="required" class="form-control">
            </div>
        </div>
        <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="password">Password <span class="required">*</span>
            </label>
            <div class="col-md-9 col-sm-9 ">
                <input type="password" id="password" name="password" required="required" class="form-control">
            </div>
        </div>
        <input type="hidden" id="role" name="role" value="2">
        <div class="ln_solid"></div>
        <div class="item form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
            </div>
        </div>
    </form>
    @endslot

</x-box-modal-large>
@foreach ($users as $user)
    <x-box-modal-edit-large id="{{$user->id}}">
        @slot('title') Edit Guru @endslot
        @slot('body')
        <form id="demo-form2" action="{{route('admin.guru.update',$user->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            @method('PUT')

            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Nama Lengkap <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="text" id="nama" name="nama" required="required" class="form-control " value="{{$user->name}}">
                </div>
            </div>

            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">Email <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="email" id="email" name="email" required="required" class="form-control" value="{{$user->email}}">
                </div>
            </div>
            <input type="hidden" id="role" name="role" value="2">
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
        @endslot
    </x-box-modal-edit-large>
@endforeach

@endsection
