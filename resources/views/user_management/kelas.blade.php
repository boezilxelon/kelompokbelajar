@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>User Management</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Kelas</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah Kelas
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Kelas</th>
                                            <th style="text-align: center; width:17%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($kelases as $kelas)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$kelas->tingkatan}} {{$kelas->nama}}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$kelas->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('admin.kelas.destroy', $kelas->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                                                
                                            </td>
                                        </tr>                   
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Siswa @endslot

        @slot('body')
        <form id="demo-form2" action="{{route('admin.kelas.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Tingkatan <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="number" id="tingkatan" name="tingkatan" required="required" class="form-control ">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Nama Kelas <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="text" id="nama" name="nama" required="required" class="form-control ">
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
        @endslot

    </x-box-modal-large>
    @foreach ($kelases as $kelas)
        <x-box-modal-edit-large id="{{$kelas->id}}">
            @slot('title') Edit Siswa @endslot
            @slot('body')
            <form id="demo-form2" action="{{route('admin.kelas.update',$kelas->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                @method('PUT')

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Tingkatan <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="number" id="tingkatan" name="tingkatan" required="required" class="form-control " value="{{$kelas->tingkatan}}">
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">Nama Kelas <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="nama" name="nama" required="required" class="form-control" value="{{$kelas->nama}}">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection
