@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>User Management</h4>
            </div>
        </div>
        
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Kelompok Siswa</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-6">
                            <table class="table  table-bordered" style="width:100%">
                                <tr>
                                    <th>Kelas</th>
                                    <th>{{$kelas->tingkatan}}{{$kelas->nama}}</th>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah Kelompok
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Nama Kelompok</th>
                                            <th style="text-align: center;">Anggota Kelompok</th>
                                            <th style="text-align: center; width:17%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($kelompoks as $kelompok)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$kelompok->name}}</td>
                                            <td style="text-align: center;">
                                                @foreach ($kelompok->userKelompok as $key => $userKelompok)
                                                    {{$key+1}}. {{$userKelompok->user->name}} <br>
                                                @endforeach
                                            
                                            
                                            </td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$kelompok->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('admin.kelompok.index.destroy', $kelompok->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                                                
                                            </td>
                                        </tr>                   
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Siswa @endslot
        @slot('body')
        <form id="demo-form2" action="{{route('admin.kelompok.index.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="name">Nama Kelompok <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="text" id="name" name="name" required="required" class="form-control ">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="name">Anggota Kelompok <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <select name="user[]" id="user" class="form-control select2" style="width:100%;" multiple required>
                        @foreach ($siswas as $siswa)
                            <option value="{{$siswa->id}}">{{$siswa->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <input type="hidden" id="kelas_id" name="kelas_id" value="{{app('request')->input('kelas_id')}}">
            
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
        @endslot
    </x-box-modal-large>

    @foreach ($kelompoks as $kelompok)
        <x-box-modal-edit-large id="{{$kelompok->id}}">
            @slot('title') Edit Kelompok @endslot
            @slot('body')
            <form id="demo-form2" action="{{route('admin.kelompok.index.update',$kelompok->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                @method('PUT')

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="name">Nama Kelompok <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="name" name="name" required="required" class="form-control " value="{{$kelompok->name}}">
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="name">Anggota Kelompok <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <select name="user[]" id="user" class="form-control select2" style="width:100%;" multiple required>
                            @foreach ($siswas as $siswa)
                                <option value="{{ $siswa->id }}" {{ in_array($siswa->id, $kelompok->userKelompok->pluck('user_id')->toArray()) ? 'selected' : '' }}>
                                    {{ $siswa->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach

@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush