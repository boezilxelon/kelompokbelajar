@extends('template.app')
@section('content')
<div class="col-md-6 col-sm-6  ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Select Data Kelompok</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <form id="demo-form2" action="{{route('admin.kelompok.index.store')}}" method="GET" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="kelas_id">Kelas <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <select name="kelas_id" id="kelas_id" class="form-control">
                        @foreach ($kelases as $kelas)
                            <option value="{{$kelas->id}}">{{$kelas->tingkatan}} {{$kelas->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>

      </div>
    </div>
  </div>
@endsection