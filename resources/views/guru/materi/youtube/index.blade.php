@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Materi</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Materi Youtube</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah Materi
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Judul</th>
                                            <th style="text-align: center; width:24%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($youtubes as $youtube)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$youtube->judul}}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$youtube->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <button type="button" class="btn btn-secondary"  data-toggle="modal" data-target="#modal-edit-lg{{'youtube'.$youtube->id}}"><i class="	fa fa-play-circle"></i> Video</button>
                                                    <form action="{{route('guru.materi.youtube.destroy', $youtube->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                                                
                                            </td>
                                        </tr>                   
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Materi Youtube @endslot

        @slot('body')
        <form id="demo-form2" action="{{route('guru.materi.youtube.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
            @csrf
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Judul <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="text" id="judul" name="judul" required="required" class="form-control ">
                </div>
            </div>
            <div class="item form-group">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Link Youtube <span class="required">*</span>
                </label>
                <div class="col-md-9 col-sm-9 ">
                    <input type="url" id="link" name="link" required="required" class="form-control ">
                </div>
            </div>
            <input type="hidden" name="kelas_id" value="{{app('request')->input('kelas_id')}}">
            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
        @endslot

    </x-box-modal-large>

    @foreach ($youtubes as $youtube)
        <x-box-modal-edit-large id="{{$youtube->id}}">
            @slot('title') Edit Materi youtube @endslot
            @slot('body')
            <form id="demo-form2" action="{{route('guru.materi.youtube.update',$youtube->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                @method('PUT')

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Judul <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="link" id="judul" name="judul" required="required" class="form-control " value="{{$youtube->judul}}">
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="email">Link Youtube <span class="required">*</span>
                    </label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="url" id="link" name="link" required="required" class="form-control" value="{{$youtube->link}}">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
            @endslot
        </x-box-modal-edit-large>
        
        <x-box-modal-edit-large id="{{'youtube'.$youtube->id}}">
            @slot('title') {{$youtube->judul}} @endslot
            @slot('body')
                <iframe id="video{{$youtube->id}}" width="100%" height="500px" src="{{$youtube->link}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection
@push('js')
    <script>
        $(document).ready(function(){
            $(".modal").on("hidden.bs.modal", function(){
                $(this).find('iframe').each(function(){
                    var src = $(this).attr('src');
                    $(this).attr('src',src);
                });
            });
        });
    </script>
@endpush