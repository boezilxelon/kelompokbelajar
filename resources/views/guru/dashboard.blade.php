@extends('template.app')

@section('content')
  <div class="row" style="display: inline-block;">
      <div class="tile_count">
          <div class="col-md-6 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-newspaper-o"></i> Total Exam Mater</span>
              <div class="count">{{$master}}</div>
          </div>
          <div class="col-md-6 col-sm-12  tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Total Exam Event</span>
              <div class="count">{{$event}}</div>
          </div>
          
          <div class="col-md-2 col-sm-4  tile_stats_count">
              
          </div>
          <div class="col-md-2 col-sm-4  tile_stats_count">
              
          </div>
      </div>
  </div>
  <!-- /top tiles -->
@endsection
