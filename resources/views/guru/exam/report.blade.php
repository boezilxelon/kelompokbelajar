@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>Report Exam</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Kelas</th>
                                            <th style="text-align: center;">Nama Siswa</th>
                                            <th style="text-align: center;">Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($reports as $report)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$report->user->kelas->tingkatan ?? ''}} {{$report->user->kelas->nama ?? ''}}</td>
                                            <td style="text-align: center;">{{$report->user->name}}</td>
                                            <td style="text-align: center;">{{$report->score}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>Grafik 5 Besar</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <canvas id="myChart" style="width: 385px; height: 100px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
          const ctx   = document.getElementById('myChart');
          var siswa   = {!!json_encode($reports->take(5)->pluck('user.name'))!!};
          var score   = {!!json_encode($reports->take(5)->pluck('score'))!!};
          var element = [];
          for (let i = 0; i < siswa.length; i++) {
            element[i] = siswa[i]+" ("+score[i]+")";
          }
          new Chart(ctx, {
            type: 'bar',
            data: {
              labels: element,
              datasets: [{
                label: 'Siswa Ranking 5 Besar',
                data: score,
                backgroundColor:['rgba(255, 99, 132, 0.6)', 'rgba(54, 162, 235, 0.6)', 'rgba(255, 206, 86, 0.6)', 'rgba(75, 192, 192, 0.6)', 'rgba(153, 102, 255, 0.6)'], // Daftar warna untuk setiap bar
                borderWidth: 1
              }]
            },
            options: {
                plugins: {
                    legend: {
                        display: false // Menghilangkan tampilan legenda
                    },
                    title: {
                        display: true,
                        text: 'Grafik Siswa dengan Nilai Tettinggi' // Judul yang ditampilkan di atas grafik
                    }
                }
            }
          });
        });

        $('#datatable').DataTable();
    </script>
@endpush