@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-6 col-sm-6  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Select Data Event</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form id="demo-form2" action="{{route('guru.exam.event.index')}}" method="GET" data-parsley-validate class="form-horizontal form-label-left">
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="year">Tahun <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <select name="year" id="year" class="form-control">
                                        @for ($year=(Carbon\Carbon::now()->year+1); $year>=2000; $year--)
                                        <option value="{{$year}}" @selected(app('request')->input('year') == $year)>{{$year}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="month">Bulan <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                <select name="month" id="month" class="form-control">
                                    <option value="01"@selected(app('request')->input('month') == 1)>Januari</option>
                                    <option value="02"@selected(app('request')->input('month') == 2)>Februari</option>
                                    <option value="03"@selected(app('request')->input('month') == 3)>Maret</option>
                                    <option value="04"@selected(app('request')->input('month') == 4)>April</option>
                                    <option value="05"@selected(app('request')->input('month') == 5)>Mei</option>
                                    <option value="06"@selected(app('request')->input('month') == 6)>Juni</option>
                                    <option value="07"@selected(app('request')->input('month') == 7)>Juli</option>
                                    <option value="08"@selected(app('request')->input('month') == 8)>Agustus</option>
                                    <option value="09"@selected(app('request')->input('month') == 9)>September</option>
                                    <option value="10"@selected(app('request')->input('month') == 10)>Oktober</option>
                                    <option value="11"@selected(app('request')->input('month') == 11)>November</option>
                                    <option value="12"@selected(app('request')->input('month') == 12)>Desember</option>
                                </select>                
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                                    <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Data Exam Master</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah  Exam Master
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Title</th>
                                            <th style="text-align: center;">Master Data</th>
                                            <th style="text-align: center;">Mulai</th>
                                            <th style="text-align: center;">Selesai</th>
                                            <th style="text-align: center; width:22%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($events as $event)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$event->name}}</td>
                                            <td style="text-align: center;">{{$event->master->name}}</td>
                                            <td style="text-align: center;">{{$event->mulai}}</td>
                                            <td style="text-align: center;">{{$event->selesai}}</td>
                                            <td style="text-align: center;"><span class="badge badge-success">Belum Selesai</span></td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$event->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('admin.exam.event.destroy', $event->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach

                                        @foreach ($dones as $event)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$event->name}}</td>
                                            <td style="text-align: center;">{{$event->master->name}}</td>
                                            <td style="text-align: center;">{{$event->mulai}}</td>
                                            <td style="text-align: center;">{{$event->selesai}}</td>
                                            <td style="text-align: center;"><span class="badge badge-danger">Selesai</span></td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$event->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('guru.exam.event.destroy', $event->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Event @endslot
        @slot('body')
            <form id="demo-form2" action="{{route('guru.exam.event.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Title <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="name" name="name" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Master Data <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <select name="master_id" id="master_id" class="form-control">
                            @foreach ($masters as $master)
                                <option value="{{$master->id}}">{{$master->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Mulai <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="datetime-local" id="mulai" name="mulai" required="required" class="form-control ">
                    </div>
                </div>
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Selesai <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="datetime-local" id="selesai" name="selesai" required="required" class="form-control ">
                    </div>
                </div>



                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        @endslot
    </x-box-modal-large>

    @foreach ($events as $event)
        <x-box-modal-edit-large id="{{$event->id}}">
            @slot('title') Edit Event @endslot
            @slot('body')
                <form id="demo-form2" action="{{route('guru.exam.event.update',$event->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                    @csrf
                    @method('PUT')
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Title <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="link" id="name" name="name" required="required" class="form-control " value="{{$event->name}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Master <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <select name="master_id" id="master_id" class="form-control">
                                @foreach ($masters as $master)
                                    <option value="{{$master->id}}" @selected($master->id == $event->master_id)>{{$master->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Mulai <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="datetime-local" id="mulai" name="mulai" required="required" class="form-control " value="{{$event->mulai}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Selesai <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="datetime-local" id="selesai" name="selesai" required="required" class="form-control " value="{{$event->selesai}}">
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                            <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
    @foreach ($dones as $event)
        <x-box-modal-edit-large id="{{$event->id}}">
            @slot('title') Edit Event @endslot
            @slot('body')
                <form id="demo-form2" action="{{route('guru.exam.event.update',$event->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                    @csrf
                    @method('PUT')
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Title <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="link" id="name" name="name" required="required" class="form-control " value="{{$event->name}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Master <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <select name="master_id" id="master_id" class="form-control">
                                @foreach ($masters as $master)
                                    <option value="{{$master->id}}" @selected($master->id == $event->master_id)>{{$master->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Mulai <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="datetime-local" id="mulai" name="mulai" required="required" class="form-control " value="{{$event->mulai}}">
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Selesai <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="datetime-local" id="selesai" name="selesai" required="required" class="form-control " value="{{$event->selesai}}">
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                            <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection