@extends('template.app')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h4>Exam</h4>
            </div>
        </div>

        <div class="x_content bs-example-popovers">
           
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                <div class="x_title">
                    <h2>List Data Exam Master</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box" style="padding-right:10px">
                                <button  type="button"  class="btn btn-info" data-toggle="modal" data-target="#modal-lg" style="float:right;" >
                                <i class="fa fa-plus"></i> Tambah  Exam Master
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;" width="5%">No</th>
                                            <th style="text-align: center;">Title</th>
                                            <th style="text-align: center;">Jumlah Soal</th>
                                            <th style="text-align: center;">Created By</th>
                                            <th style="text-align: center; width:22%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($masters as $master)
                                        <tr>
                                            <td style="text-align: center;">{{++$i}}</td>
                                            <td style="text-align: center;">{{$master->name}}</td>
                                            <td style="text-align: center;">{{$master->question->count()}} Soal</td>
                                            <td style="text-align: center;">{{$master->user->name}}</td>
                                            <td style="text-align: center;">
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <a href="{{ route('guru.exam.question.index', ['master_id' => $master->id]) }}">
                                                        <button type="button" class="btn btn-secondary"><i class="fa fa-folder"></i> Soal</button>
                                                    </a>
                                                    <button type="button" class="btn btn-warning text-white"  data-toggle="modal" data-target="#modal-edit-lg{{$master->id}}"><i class="fa fa-pencil"></i> Ubah</button>
                                                    <form action="{{route('guru.exam.master.destroy', $master->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-box-modal-large>
        @slot('title') Tambah Exam Master @endslot
        @slot('body')
            <form id="demo-form2" action="{{route('guru.exam.master.store')}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                @csrf
                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Title <span class="required">*</span></label>
                    <div class="col-md-9 col-sm-9 ">
                        <input type="text" id="name" name="name" required="required" class="form-control ">
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                        <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        @endslot
    </x-box-modal-large>

    @foreach ($masters as $master)
        <x-box-modal-edit-large id="{{$master->id}}">
            @slot('title') Edit Exam Master @endslot
            @slot('body')
                <form id="demo-form2" action="{{route('guru.exam.master.update',$master->id)}}" method="POST" data-parsley-validate class="form-horizontal form-label-left">
                    @csrf
                    @method('PUT')
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="nama">Title <span class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="link" id="name" name="name" required="required" class="form-control " value="{{$master->name}}">
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info"  style="float: right;">Submit</button>
                            <button type="button" class="btn btn-secondary"  style="float: right;" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            @endslot
        </x-box-modal-edit-large>
    @endforeach
@endsection